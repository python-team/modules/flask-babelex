Source: flask-babelex
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Christoph Berg <myon@debian.org>
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-babel,
 python3-flask,
 python3-setuptools,
Standards-Version: 4.6.0
Homepage: https://github.com/mrjoes/flask-babelex
Vcs-Git: https://salsa.debian.org/python-team/packages/flask-babelex.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/flask-babelex

Package: python3-flask-babelex
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Adds i18n/l10n support to Flask applications
 Flask-BabelEx adds i18n/l10n support to Flask applications with the help of
 the Babel library. It is a fork of the official Flask-Babel extension with
 the following features:
 .
  1. It is possible to use multiple language catalogs in one Flask application;
  2. Localization domains: your extension can package localization file(s) and
     use them if necessary;
  3. Does not reload localizations for each request.
 .
 Flask-BabelEx is API-compatible with Flask-Babel.
